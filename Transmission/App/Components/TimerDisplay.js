import React, { Component } from 'react';

import {
  View,
  Text,
} from 'react-native';

export class TimerDisplay extends Component<{}> {

  constructor(props) {
    super(props);
    this.tutorielPointStep = 0;
    this.currentRatioLeft = 1;
    this.currentTimeLeft = 0;
    this.timeoutReached = props.timeoutReached;
  }
  cancelTimeout() {
    this.tutorielPointStep = 0;
    this.currentRatioLeft = 1;
    this.currentTimeLeft = 0;
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.forceUpdate();
  }
  setTimeout(timeoutInSeconds) {
    if (this.interval) {
      clearInterval(this.interval);
    }
    this.dateTimeoutStarted = (new Date()).getTime();
    this.initialTimeout = timeoutInSeconds;
    this.currentTimeLeft = timeoutInSeconds;
    this.currentRatioLeft = 1;
    this.interval = setInterval(() => {
      let elapsedTime = (new Date()).getTime() - this.dateTimeoutStarted;

      this.currentTimeLeft = this.initialTimeout - elapsedTime;
      this.currentRatioLeft = this.currentTimeLeft / this.initialTimeout;
      this.forceUpdate();
      if (this.currentTimeLeft <= 0) {
        if (this.timeoutReached) {
          this.timeoutReached();
        }
        clearInterval(this.interval);
        return;
      }
    }, 1 / 30 * 1000);
  }

  getHeight() {
    if (this.currentTimeLeft && this.currentTimeLeft > 0) {
      return 20;
    }
    return 0;
  }

  render() {
    return (
      <View style={{height: this.getHeight()}}>
        <Text style={{height: this.getHeight()}}>Plus que {Math.ceil(this.currentTimeLeft / 1000)} seconde{this.currentTimeLeft > 1000 ? 's' : ''} pour répondre !</Text>
        <View ref='timerContainer' style={{height: this.getHeight(), flexDirection: 'row' }}>
          <View style={{
            flex: this.currentRatioLeft,
            height: this.getHeight(),
            backgroundColor: 'rgb(' + (255 - this.currentRatioLeft * 255) + ', '
              + this.currentRatioLeft * 255 + ', ' + 0 + ')'
          }} />
        </View>
      </View>
    );
  }
}