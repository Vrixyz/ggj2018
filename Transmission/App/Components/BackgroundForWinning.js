import React, { Component } from 'react';

import {
    Animated,
    View,
    StyleSheet,
    Easing,
    Dimensions,
} from 'react-native';

const circle1width = 100;
const circle1InitialYPosition = -150;
const circle2InitialYPosition = -150;

export class BackgroundForWinning extends Component<{}> {
    state = {
        yPositionCircle1: new Animated.Value(circle1InitialYPosition),
        yPositionCircle2: new Animated.Value(circle2InitialYPosition),
        dimensions: Dimensions.get('window')
    }
    constructor(props) {
        super(props);
    }
    animateCircle1() {
        let circle1Duration = 5000;
        this.state.yPositionCircle1.setValue(circle1InitialYPosition);
        Animated.timing(this.state.yPositionCircle1, {
            toValue: this.state.dimensions.height,
            //easing: Easing.back(),
            duration: circle1Duration,
        }).start();
        
        setTimeout(() => {
            this.animateCircle1();
        }, circle1Duration + (Math.random() % (circle1Duration / 2)));
    }
    animateCircle2() {
        let circle2Duration = 7000 - Math.random() % 2000;
        this.state.yPositionCircle2.setValue(circle2InitialYPosition);
        Animated.timing(this.state.yPositionCircle2, {
            toValue: this.state.dimensions.height,
            //easing: Easing.back(),
            duration: circle2Duration,
        }).start();
        
        setTimeout(() => {
            this.animateCircle2();
        }, circle2Duration + (Math.random() % (circle2Duration / 2)));
    }
    componentDidMount() {
        this.animateCircle1();
        this.animateCircle2();
    }
    render() {
        return (
            <View style={{ position: 'absolute' }}>
                <Animated.View style={{
                    position: 'absolute',
                    width: circle1width,
                    height: circle1width,
                    borderRadius: circle1width / 2,
                    backgroundColor: 'yellow',
                    top: this.state.yPositionCircle1,
                    left: -this.state.dimensions.width / 2 + 10,
                }} />
                <Animated.View style={{
                    position: 'absolute',
                    width: circle1width,
                    height: circle1width,
                    borderRadius: circle1width / 2,
                    backgroundColor: 'yellow',
                    top: this.state.yPositionCircle2,
                    left: 20
                }} />
            </View>
        );
    }
}

