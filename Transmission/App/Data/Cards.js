export const AnswerType = Object.freeze({CORRECT:2, WRONG:3, WIN: 4});

const card_bloodyCaesar = {
    question: "Qu'appelle t'on 'Bloody Caesar' ?",
    answers: [
        { title: "Une salade"},
        { title: "Un cocktail alcoolisé", type: AnswerType.CORRECT, nextCard: "card_alcoholic" },
        { title: "Une célèbre bataille", nextCard: "badCards_bloodyCaesar[0]" }]
};
const card_alcoholic = {
    question: "Expression désignant quelqu’un d’alcoolisé :",
    answers: [
        { title: "Chauffé à bloc" },
        { title: "Rond comme un ballon", type: AnswerType.CORRECT, nextCard: "card_ball" },
        { title: "Être à sec" }],
    points: 2,
};
const card_ball = {
    question: "Quel sport se joue avec un ballon ?",
    answers: [
        { title: "Le Ping-Pong" },
        { title: "Le Football", type: AnswerType.CORRECT, nextCard: "card_footballPlayer" },
        { title: "Les Échecs" }]
};
const card_footballPlayer = {
    question: "Footballeur connu :",
    answers: [
        { title: "David Beckham", type: AnswerType.CORRECT, nextCard: "card_pujadas" },
        { title: "Jean Paul Sartre" },
        { title: "Jean Dujardin" }]
};
const card_pujadas = {
    question: "Pourquoi David Pujadas est-il connu ?",
    answers: [
        { title: "Il est champion de karaté" },
        { title: "Pour ses talents d’acteurs dans “amour, gloire et beauté”" },
        { title: "Pour ses nombreuses émissions en tant que présentateur de journal télévisé", type: AnswerType.CORRECT, nextCard: "card_news" }]
};

const card_news = {
    question: "Laquelle de ces propositions est un journal d’information ?",
    answers: [
        { title: "20 minutes", type: AnswerType.CORRECT, nextCard: "card_minutes" },
        { title: "La Planète" },
        { title: "Le canard embourbé" }]
};
const card_minutes = {
    question: "Que représente 525600 minutes ?",
    answers: [
        { title: "1 année", type: AnswerType.CORRECT, nextCard: "card_year" },
        { title: "10 mois" },
        { title: "400 jours" }],
    timeout:8000
};

const card_year = {
    question: "En quelle année somme nous ?",
    answers: [
        { title: "2018", type: AnswerType.CORRECT, nextCard:"card_gameJamTheme" },
        { title: "2017" },
        { title: "2019" }]
};

const card_gameJamTheme = {
    question: "Quel était le thème de la global game jam 2018 ?",
    answers: [
        { title: "Transmission", type: AnswerType.WIN },
        { title: "Waves" },
        { title: "Radio" }]
};

export const cards = [card_bloodyCaesar, card_alcoholic, card_ball, card_footballPlayer, card_pujadas, card_news, card_minutes, card_year, card_gameJamTheme];

const badCards_bloodyCaesar = [{
    question: "Qui a gagné la bataille de Strasbourg en 357 ?",
    answers: [
        { title: "Jules César" },
        { title: "Julien l’Apostat", type: AnswerType.CORRECT , nextCard: "badCards_bloodyCaesar[1]" },
        { title: "Valentinien 1er" }]
},
{
    question: "Que devient le vin lors de l’eucharistie chrétienne ?",
    answers: [
        { title: "Le sang du christ", type: AnswerType.CORRECT, nextCard: "card_bloodyCaesar" },
        { title: "Du jus d’orange" },
        { title: "Le corp du christ" }]
}];

// Eval all nextCards to update their references

function updateAnswer(answer) {
    if (answer.nextCard) {
        answer.nextCard = eval(answer.nextCard);
    }
    if (answer.type == undefined) {
        answer.type = AnswerType.WRONG;
    }
}

badCards_bloodyCaesar.forEach(card => {
    card.answers.forEach(answer => {
        updateAnswer(answer);
    });
});

cards.forEach(card => {
    card.answers.forEach(answer => {
        updateAnswer(answer);
    });
});