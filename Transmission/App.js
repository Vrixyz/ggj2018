/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

import { StackNavigator } from 'react-navigation';

import { cards, AnswerType } from './App/Data/Cards'

import { TimerDisplay } from './App/Components/TimerDisplay'
import { BackgroundForWinning } from './App/Components/BackgroundForWinning'

/**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 * https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
 */
function shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

function randomElementFromArray(array) {
  return array[Math.floor(Math.random() * array.length)]
}

const alert_answers_correct = ["C'est la bonne réponse !", "Ouiii", "Continue comme ca !", "Tu es bon à ca !", "Correct !"]
const alert_answers_correct_points_tutoriel = ["Bravo, regarde tu as gagné des points !", "Encore correct, encore des points ! Mais à quoi servent t'ils ?", "Encore des points, plus tard tu pourra acheter des pouvoirs avec !"];

/// Props :
/// - card : card object
/// - navigation
/// - lastAnswerType : AnswerType, to show a shiny animation or flavour text
class AnswerCard extends Component<{}> {

  constructor(props) {
    shuffle(props.card.answers);
    super(props);
    this.setValues(this.props.card, this.props.lastAnswerType);
    this.tutorielPointStep = 0;
  }

  setValues(newCard, guessedAnswerType) {
    this.card = newCard;
    this.lastAnswerType = guessedAnswerType;
    if (this.card.timeout) {
      setTimeout(() => {
        this.timerDisplay.setTimeout(this.card.timeout);
      }, 100);
    }
    else {
      setTimeout(() => {
        this.timerDisplay.cancelTimeout();
      }, 100);
    }
  }
  onPressAnswer(answer) {
    if (answer.type == AnswerType.WIN) {
      alert("Gagné !");
      this.props.navigation.navigate("Home");
      return;
    }
    if (answer.nextCard) {
      let stringToDisplay = undefined;
      if (answer.type == AnswerType.CORRECT) {
        stringToDisplay = alert_answers_correct[Math.floor(Math.random() * alert_answers_correct.length)];;
        if (this.card.points) {
          this.totalPoints = (this.totalPoints ? (this.totalPoints + this.card.points) : this.card.points);
          if (this.tutorielPointStep < alert_answers_correct_points_tutoriel.length) {
            stringToDisplay = alert_answers_correct_points_tutoriel[this.tutorielPointStep];
            this.tutorielPointStep++;
          }
        }
      }
      else if (answer.type == AnswerType.WRONG) {
        stringToDisplay = "Non, essaie encore...";
      }
      alert(stringToDisplay);
      this.setValues(answer.nextCard, answer.type);
      this.forceUpdate();
      return;
    }
    if (answer.type == AnswerType.WRONG) {
      alert("Perdu...");
      this.props.navigation.goBack();
    }
  }

  timeoutReached() {
    alert("Tu as été trop leeeeent !!!");
    this.props.navigation.goBack();
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={{ flex: 0.2 }} />
        <Text style={styles.question}>
          {this.card.question}
        </Text>
        <View style={{ flex: 0.2 }} />
        <Button title={this.card.answers[0].title} onPress={this.onPressAnswer.bind(this, this.card.answers[0])} style={styles.answer} />
        <Button title={this.card.answers[1].title} onPress={this.onPressAnswer.bind(this, this.card.answers[1])} style={styles.answer} />
        <Button title={this.card.answers[2].title} onPress={this.onPressAnswer.bind(this, this.card.answers[2])} style={styles.answer} />
        <TimerDisplay timeoutReached={() => { this.timeoutReached() }} ref={(timerDisplay) => { this.timerDisplay = timerDisplay; }} />
        <View style={{ height: 20 }} />
        <Text>{((this.totalPoints > 0) ? this.totalPoints + " points" : "")}</Text>
      </View>
    );
  }
}

/// Params passed by navigation should be :
/// - card containing a card object
class App extends Component<{}> {

  render() {
    return (
      <AnswerCard card={this.props.navigation.state.params.card} lastAnswerType={this.props.navigation.state.params.lastAnswerType} navigation={this.props.navigation} />
    );
  }
}

const HomeScreen = ({ navigation }) => (
  <View style={styles.container}>
    <BackgroundForWinning />
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        onPress={() => navigation.navigate('Game', { card: cards[0] })}
        title="Jouer !"
      />
    </View>
  </View>
);

const RootNavigator = StackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: {
      headerTitle: 'Transmission',
      headerLeft: null
    },
  },
  Game: {
    screen: App,
    navigationOptions: {
      headerTitle: 'Transmission',
      headerLeft: null
    },
  }
});

export default RootNavigator;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  answer: {
    color: '#F5FCFF',
  },
  question: {
    fontSize: 30,
    flex: 0.25,
    textAlign: 'center',
    color: 'blue',
  },
});
